import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("dirname", ["/etc/rundeck", "/var/lib/rundeck"])
def test_directories(host, dirname):
    d = host.file(dirname)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize(
    "name,content",
    [
        ("/etc/default/rundeckd", "Xmx"),
        (
            "/etc/rundeck/rundeck-config.properties",
            "rundeck.security.authorization.preauthenticated.enabled",
        ),
    ],
)
def test_files(host, name, content):
    f = host.file(name)
    assert f.exists
    if content:
        assert f.contains(content)


@pytest.mark.parametrize("name", ["rundeckd", "nginx", "caja"])
def test_services(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled


def test_sockets(host):
    assert host.socket("tcp://127.0.0.1:80").is_listening
    assert host.socket("tcp://127.0.0.1:5001").is_listening
    assert host.socket("tcp://127.0.0.1:4440").is_listening

    assert not host.socket("tcp://0.0.0.0:4440").is_listening
    assert not host.socket("tcp://0.0.0.0:5001").is_listening
    assert not host.socket("tcp://0.0.0.0:80").is_listening

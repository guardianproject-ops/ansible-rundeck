## Role Variables

* `rundeck_version`: `3.3.2.20200817-1` - the rundeck version to install



* `rundeck_caja_version`: `0.0.4` - the version of caja to install



* `rundeck_cf_access_audience`: `` - the cloudflare access policy audience



* `rundeck_cf_access_domain`: `` - the cloudflare access domain, e.g., example.cloudflareaccess.com



* `rundeck_domain`: `` - the domain for your rundeck instance, e.g., rundeck.example.com



* `rundeck_plugins`: `see default/main.yml` - a list of urls to plugin JAR files to install



* `rundeck_xmx`: `2048` - the explicit max heap size for the jvm, in megabytes. do not add the units suffix.



* `rundeck_xms`: `1024` - the explicit min heap size for the jvm, in megabytes. do not add the units suffix.



* `rundeck_maxmetaspacesize`: `512` - the max metaspace size for the jvm, in megabytes. do not add the units suffix.



* `rundeck_url`: `https://{{ rundeck_domain }}` - the full url rundeck will be available at



* `rundeck_jdbc_driver_classname`: `` - supply the jdbc driver class name



* `rundeck_jdbc_uri`: `` - supply a full jdbc connection uri for rundeck, if not supplied, the H2 database will be used



* `rundeck_jdbc_username`: `` - the database connection username



* `rundeck_jdbc_password`: `` - the database connection password



* `rundeck_config`: `see defaults/main.yml` - The config for rundeck, stored in /etc/rundeck/rundeck-config.properties



* `rundeck_framework`: `see defaults/main.yml` - The settings for rundeck, stored in /etc/rundeck/framework.properties



* `rundeck_users`: `[]` - the default rundeck users to include, see rundeck docs for more options like hashing


  ```yaml
  - username: "admin"
     password: "admin"
     roles: "user,admin"
   - username: "user"
     password: "user"
     roles: "user"
